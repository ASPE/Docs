-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: saic
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `matricula` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno`
--

LOCK TABLES `aluno` WRITE;
/*!40000 ALTER TABLE `aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartaoresposta`
--

DROP TABLE IF EXISTS `cartaoresposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartaoresposta` (
  `id` int(11) NOT NULL,
  `resposta` int(11) DEFAULT NULL,
  `id_questao` int(11) DEFAULT NULL,
  `id_modelo_prova` int(11) DEFAULT NULL,
  `matriculaAluno` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cartao_opcao_idx` (`resposta`),
  KEY `fk_cartao_modelo_idx` (`id_modelo_prova`),
  KEY `fk_cartao_questao_idx` (`id_questao`),
  CONSTRAINT `fk_cartao_opcao` FOREIGN KEY (`resposta`) REFERENCES `opcao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cartao_questao` FOREIGN KEY (`id_questao`) REFERENCES `questao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cartao_modelo` FOREIGN KEY (`id_modelo_prova`) REFERENCES `modeloprova` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartaoresposta`
--

LOCK TABLES `cartaoresposta` WRITE;
/*!40000 ALTER TABLE `cartaoresposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartaoresposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `sigla` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplina`
--

LOCK TABLES `disciplina` WRITE;
/*!40000 ALTER TABLE `disciplina` DISABLE KEYS */;
/*!40000 ALTER TABLE `disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instituicao`
--

DROP TABLE IF EXISTS `instituicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `sigla` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituicao`
--

LOCK TABLES `instituicao` WRITE;
/*!40000 ALTER TABLE `instituicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `instituicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modeloprova`
--

DROP TABLE IF EXISTS `modeloprova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modeloprova` (
  `id` int(11) NOT NULL,
  `num_questoes` int(11) DEFAULT NULL,
  `data_inicio` datetime DEFAULT NULL,
  `data_fim` datetime DEFAULT NULL,
  `voltar` tinyint(1) DEFAULT NULL,
  `duracao` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modeloprova`
--

LOCK TABLES `modeloprova` WRITE;
/*!40000 ALTER TABLE `modeloprova` DISABLE KEYS */;
/*!40000 ALTER TABLE `modeloprova` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcao`
--

DROP TABLE IF EXISTS `opcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opcao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `id_questao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idtable1_UNIQUE` (`id`),
  KEY `fk_opcao_qustao_idx` (`id_questao`),
  CONSTRAINT `fk_opcao_questao` FOREIGN KEY (`id_questao`) REFERENCES `questao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcao`
--

LOCK TABLES `opcao` WRITE;
/*!40000 ALTER TABLE `opcao` DISABLE KEYS */;
/*!40000 ALTER TABLE `opcao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesodisciplina`
--

DROP TABLE IF EXISTS `pesodisciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesodisciplina` (
  `id_modelo_prova` int(11) NOT NULL,
  `id_disciplina` int(11) NOT NULL,
  PRIMARY KEY (`id_modelo_prova`,`id_disciplina`),
  KEY `fk_peso_disciplina_idx` (`id_disciplina`),
  CONSTRAINT `fk_modelo_prova` FOREIGN KEY (`id_modelo_prova`) REFERENCES `modeloprova` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peso_disciplina` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesodisciplina`
--

LOCK TABLES `pesodisciplina` WRITE;
/*!40000 ALTER TABLE `pesodisciplina` DISABLE KEYS */;
/*!40000 ALTER TABLE `pesodisciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesotopico`
--

DROP TABLE IF EXISTS `pesotopico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesotopico` (
  `id_topico` int(11) NOT NULL,
  `id_modelo_prova` int(11) NOT NULL,
  `peso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_topico`,`id_modelo_prova`),
  KEY `fk_topico_modelo_idx` (`id_modelo_prova`),
  CONSTRAINT `fk_topico_peso` FOREIGN KEY (`id_topico`) REFERENCES `topico` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_topico_modelo` FOREIGN KEY (`id_modelo_prova`) REFERENCES `modeloprova` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesotopico`
--

LOCK TABLES `pesotopico` WRITE;
/*!40000 ALTER TABLE `pesotopico` DISABLE KEYS */;
/*!40000 ALTER TABLE `pesotopico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questao`
--

DROP TABLE IF EXISTS `questao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enunciado` varchar(500) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `trasnversal` tinyint(1) DEFAULT NULL,
  `resposta` int(11) DEFAULT NULL,
  `id_instituicao` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questao_instituicao_idx` (`id_instituicao`),
  KEY `fk_questao_opcao_resposta_idx` (`resposta`),
  KEY `fk_questao_status_idx` (`id_status`),
  CONSTRAINT `fk_questao_instituicao` FOREIGN KEY (`id_instituicao`) REFERENCES `instituicao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_questao_opcao_resposta` FOREIGN KEY (`resposta`) REFERENCES `opcao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_questao_status` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questao`
--

LOCK TABLES `questao` WRITE;
/*!40000 ALTER TABLE `questao` DISABLE KEYS */;
/*!40000 ALTER TABLE `questao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questaoobrigatoria`
--

DROP TABLE IF EXISTS `questaoobrigatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questaoobrigatoria` (
  `id_questao` int(11) NOT NULL,
  `id_modelo_prova` int(11) NOT NULL,
  PRIMARY KEY (`id_questao`,`id_modelo_prova`),
  KEY `fk_questao_obrigatoria_modelo_idx` (`id_modelo_prova`),
  CONSTRAINT `fk_questao_obrigatoria` FOREIGN KEY (`id_questao`) REFERENCES `questao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_questao_obrigatoria_modelo` FOREIGN KEY (`id_modelo_prova`) REFERENCES `modeloprova` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questaoobrigatoria`
--

LOCK TABLES `questaoobrigatoria` WRITE;
/*!40000 ALTER TABLE `questaoobrigatoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `questaoobrigatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realizacaoprova`
--

DROP TABLE IF EXISTS `realizacaoprova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realizacaoprova` (
  `id_cartao_resposta` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `data_inicio` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cartao_resposta`,`id_aluno`),
  KEY `fk_aluno_idx` (`id_aluno`),
  CONSTRAINT `fk_cartao_resposta` FOREIGN KEY (`id_cartao_resposta`) REFERENCES `cartaoresposta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_aluno` FOREIGN KEY (`id_aluno`) REFERENCES `aluno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realizacaoprova`
--

LOCK TABLES `realizacaoprova` WRITE;
/*!40000 ALTER TABLE `realizacaoprova` DISABLE KEYS */;
/*!40000 ALTER TABLE `realizacaoprova` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topico`
--

DROP TABLE IF EXISTS `topico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topico`
--

LOCK TABLES `topico` WRITE;
/*!40000 ALTER TABLE `topico` DISABLE KEYS */;
/*!40000 ALTER TABLE `topico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topicodaquestao`
--

DROP TABLE IF EXISTS `topicodaquestao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topicodaquestao` (
  `id_topico` int(11) NOT NULL,
  `id_questao` int(11) NOT NULL,
  PRIMARY KEY (`id_topico`,`id_questao`),
  KEY `fk_tq_questao_idx` (`id_questao`),
  CONSTRAINT `fk_tq_topicos` FOREIGN KEY (`id_topico`) REFERENCES `topico` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tq_questao` FOREIGN KEY (`id_questao`) REFERENCES `questao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topicodaquestao`
--

LOCK TABLES `topicodaquestao` WRITE;
/*!40000 ALTER TABLE `topicodaquestao` DISABLE KEYS */;
/*!40000 ALTER TABLE `topicodaquestao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topicodisciplina`
--

DROP TABLE IF EXISTS `topicodisciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topicodisciplina` (
  `id_topico` int(11) NOT NULL,
  `id_disciplina` int(11) NOT NULL,
  PRIMARY KEY (`id_topico`,`id_disciplina`),
  KEY `fk_disciplina_topico_idx` (`id_disciplina`),
  CONSTRAINT `fk_topico` FOREIGN KEY (`id_topico`) REFERENCES `topico` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disciplina_topico` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topicodisciplina`
--

LOCK TABLES `topicodisciplina` WRITE;
/*!40000 ALTER TABLE `topicodisciplina` DISABLE KEYS */;
/*!40000 ALTER TABLE `topicodisciplina` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-31 20:34:28
