# Arquivos do Docs


### Descrição dos arquivos
+ **DIAGRAMAS.asta** - Possui todos os diagramas em formato.asta

+  **AnteProjeto-SAIC.doc** - Possui o anteprojeto

+  **ER - QUESTOES.odt** - Especificação de requisitos do sistema de questões do SAIC, possui a descrição dos casos de uso

+  **SAIC.mwb** - Esquema de banco de dados em formato workbench
